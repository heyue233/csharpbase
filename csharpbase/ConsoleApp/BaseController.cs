﻿using System;
using System.Collections.Generic;
using System.Text;
using com.heyue233;
using com.heyue233.csharpbase;

namespace ConsoleApp
{
    class BaseController
    {
        private Program program = new Program();

        private Client client = new Client();

        public Client GetClient()
        {
            return client;
        }

        public void SetClient(Client value)
        {
            client = value;
        }

        public Program GetProgram()
        {
            return program;
        }

        public void SetProgram(Program value)
        {
            program = value;
        }
    }
}
