﻿using System.Drawing;
using System.Windows.Forms;
using System;
namespace WindowsFormsApp
{
    /// <summary>
    /// Application.CurrentDomain.BaseDirectory
    /// </summary>
    public class Form2 : Form
    {
        private System.ComponentModel.IContainer components = null;

        private Button button1;

        private TextBox TextBox_ImageName;

        private OpenFileDialog OpenFileDialog_ofg;

        private PictureBox PictureBox_HeadIamge;

        public Form2()
        {
            InitializeComponent();
            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
            
        }

        /// <summary>
        /// 初始化组件
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new Button();
            // 绑定事件
            button1.Click += SelectImageFile;


            TextBox_ImageName = new TextBox();
            TextBox_ImageName.Location = new Point(3, 50);
            TextBox_ImageName.Name = "TextBox_ImageName";
            TextBox_ImageName.Size = new Size(300,25);
            this.Controls.Add(TextBox_ImageName);

            PictureBox_HeadIamge = new PictureBox();
            PictureBox_HeadIamge.Location = new Point(3, 100);
            PictureBox_HeadIamge.Size = new Size(300,300);
            PictureBox_HeadIamge.BorderStyle = BorderStyle.FixedSingle;
            PictureBox_HeadIamge.SizeMode = PictureBoxSizeMode.StretchImage;
            PictureBox_HeadIamge.BackgroundImageLayout = ImageLayout.Stretch;
            Controls.Add(PictureBox_HeadIamge);

            OpenFileDialog_ofg = new OpenFileDialog();
            OpenFileDialog_ofg.Title = "选择图片文件";
            OpenFileDialog_ofg.Filter = "图片文件|*.jpg|所有文件|*.*";
            //OpenFileDialog_ofg.
            //ofg.ShowDialog
            // 临时挂起控件，作用是：避免当控件很多时，反复重绘界面
            // 挂起所有控件，在ResumeLayout恢复布局时，一起执行，这样可以提升程序性能
            //SuspendLayout();

            //button1.Dock = DockStyle.Left;
            button1.Location = new Point(3, 3);
            //button1.Name = "button1";
            //button1.Size = new Size()
            button1.TabIndex = 0;
            button1.Text = "按钮";


            Controls.Add(this.button1);

            //this.AutoScaleDimensions = new SizeF(120F, 120F);
            //this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(800, 500);
            this.Name = "Form2";
            this.Text = "Form2";
            //this.ResumeLayout(false);
            this.PerformLayout();
        }

        private void SelectImageFile(object sender, System.EventArgs e)
        {
            if (OpenFileDialog_ofg.ShowDialog() == DialogResult.OK)
            {
                // 获取文件的文件名
                TextBox_ImageName.Text = OpenFileDialog_ofg.FileName;

                Image image = Image.FromFile(OpenFileDialog_ofg.FileName);
                PictureBox_HeadIamge.Image = image;
            }
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
