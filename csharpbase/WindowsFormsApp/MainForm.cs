﻿using System;
using System.Windows.Forms;

/// <summary>
/// 快捷键
/// ctrl + k, d 代码对齐
/// ctrl + k, c 选中行注释
/// ctrl + k, u 选中行取消注释
/// </summary>
namespace WindowsFormsApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        ///  窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            Console.WriteLine("窗体被加载");
        }

        /// <summary>
        /// 窗体关闭之后的事件，窗体关闭的事件（Closed和Closing）只能由窗体本身驱动<br/>
        /// 如果是通过任务管理器进行关闭，不会触发该事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Console.WriteLine("窗体已经被关闭");
        }

        /// <summary>
        /// 窗体正在关闭的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Console.WriteLine("窗体正在关闭");
        }

        /// <summary>
        /// 窗体单击和双击只能针对在窗体中，如果在标题栏是无效的
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Click(object sender, EventArgs e)
        {
            Console.WriteLine("窗体单击事件");
        }

        /// <summary>
        /// 双击会触发一次单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_DoubleClick(object sender, EventArgs e)
        {
            Console.WriteLine("窗体双击事件");
        }

        /// <summary>
        /// 键被按下触发的事件（一直按着会一直触发）
        /// KeyCode是被按下键的Keys对象，KeyValue返回的是键的ASCLL码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine($"窗体KeyDown事件：keyCode [{e.KeyCode}], keyValue [{e.KeyValue}]");
        }

        /// <summary>
        /// 键被按下和弹起时触发（只触发一次，一直按会一直触发，顺序在KeyDown之后）
        /// KeyChar为按键的字符char
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            Console.WriteLine($"窗体KeyPress事件：KeyChar [{e.KeyChar}]");
        }

        /// <summary>
        /// 按键弹起时（释放）触发，在KeyPress之后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {

            Console.WriteLine($"窗体KeyUp事件：keyCode [{e.KeyCode}], keyValue [{e.KeyValue}]");
        }
    }
}
